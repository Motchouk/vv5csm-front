import React from 'react';
import { LoginForm } from '../../2-Organisms/LoginForm/LoginForm';
import './Login.scss';

export const Login = () => {
    return <>
        <h1 className="login-title">Login</h1>
        <LoginForm></LoginForm>
    </>
}