import React, {useState} from 'react'
import { FormField } from '../../1-Molecules/FormField/FormField'
import { Message } from '../../0-Atoms/Message/Message'
import { login } from '../../../utils/JWTAuth'
import './LoginForm.scss'

export const LoginForm = () => {
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();
    const [message, setMessage] = useState();
    const [text, setText] = useState();

    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        console.log("form submitted.");
        // TODO :
        // 1 Récupérer le username et password saisi
        let data = {
            username: email,
            password: password,
        }
        // 2 Appeler la fonction login
        login(data).then(response => {
            setMessage("success");
            setText("Vous êtes connecté !");
        }).catch (e => {
            setMessage("error");
            setText("Identifiant ou mot de passe invalide.");
        });
    }

    const handleTextFieldChange = (mySetFunction: any, event: any) => {
        const value = event.currentTarget.value;
        mySetFunction(value);
    }

    return (
        <form className='login-form-container' onSubmit={event => handleSubmit(event)}>
            <Message
                type={message}
                text={text}
            />
            <FormField
                htmlFor={'email'}
                label={'Email'}
                inputType={'email'}
                name={'email'}
                value={email}
                placeholder={'Adresse mail'}
                messageType={''}
                text={''}
                onChange={(e: any) => handleTextFieldChange(setEmail, e)}
            />
            <FormField
                htmlFor={'password'}
                label={'Password'}
                inputType={'password'}
                name={'password'}
                value={password}
                placeholder={'Mot de passe'}
                messageType={''}
                text={''}
                onChange={(e: any) => handleTextFieldChange(setPassword, e)}
            />
            <input
                className='submitButton'
                type='submit'
                value='Login'
                onClick={(e: any) => handleSubmit}
            />
        </form>
    )
}