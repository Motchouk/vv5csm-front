import React, { useState } from 'react'
import {AttributeLine} from './../../1-Molecules/AttributeLine/AttributeLine'
import {SectionTitle} from './../../0-Atoms/SectionTitle/SectionTitle'
import './Attributes.scss'


export const Attributes = () => {
    const radioSize = 5;
    const separatorType = 'default';
    const [strength, setStrength] = useState(1);
    const [dexterity, setDexterity] = useState(1);
    const [stamina, setStamina] = useState(1);
    const [charisma, setCharisma] = useState(1);
    const [manipulation, setManipulation] = useState(1);
    const [composure, setComposure] = useState(1);
    const [intelligence, setIntelligence] = useState(1);
    const [wits, setWits] = useState(1);
    const [resolve, setResolve] = useState(1);

    return <>
        <SectionTitle title={"Attributes"}/>
        <div className='attributes-container'>
            <div className='attributes-physical'>
            <h3>Physical</h3>
                <AttributeLine label='Strength' size={radioSize} level={strength} type={separatorType}></AttributeLine>
                <AttributeLine label='Dexterity' size={radioSize} level={dexterity} type={separatorType}></AttributeLine>
                <AttributeLine label='Stamina' size={radioSize} level={stamina} type={separatorType}></AttributeLine>
            </div>
            <div className='attributes-social'>
            <h3>Social</h3>
                <AttributeLine label='Charisma' size={radioSize} level={charisma} type={separatorType}></AttributeLine>
                <AttributeLine label='Manipulation' size={radioSize} level={manipulation} type={separatorType}></AttributeLine>
                <AttributeLine label='Composure' size={radioSize} level={composure} type={separatorType}></AttributeLine>
            </div>
            <div className='attributes-mental'>
            <h3>Mental</h3>
                <AttributeLine label='Intelligence' size={radioSize} level={intelligence} type={separatorType}></AttributeLine>
                <AttributeLine label='Wits' size={radioSize} level={wits} type={separatorType}></AttributeLine>
                <AttributeLine label='Resolve' size={radioSize} level={resolve} type={separatorType}></AttributeLine>
            </div>
        </div>
    </>
}
