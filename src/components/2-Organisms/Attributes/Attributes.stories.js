import React from 'react';
import { Attributes } from './Attributes';

export default {
    title : 'Attributes',
    component: Attributes,
};

export const Default = () => <Attributes/>