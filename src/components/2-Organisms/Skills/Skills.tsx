import React, { useState } from 'react'
import {AttributeLine} from './../../1-Molecules/AttributeLine/AttributeLine'
import {SectionTitle} from './../../0-Atoms/SectionTitle/SectionTitle'
import './Skills.scss'

export const Skills = () => {
    const radioSize = 5;
    const separatorType = 'dotted';
    const [athletics, setAthletics] = useState(0);
    const [brawl, setBrawl] = useState(0);
    const [craft, setCraft] = useState(0);
    const [drive, setDrive] = useState(0);
    const [firearms, setFirearms] = useState(0);
    const [melee, setMelee] = useState(0);
    const [larceny, setLarceny] = useState(0);
    const [stealth, setStealth] = useState(0);
    const [survival, setSurvival] = useState(0);
    const [animalKen, setAnimalKen] = useState(0);
    const [etiquette, setEtiquette] = useState(0);
    const [insight, setInsight] = useState(0);
    const [intimidation, setIntimidation] = useState(0);
    const [leadership, setLeadership] = useState(0);
    const [performance, setPerformance] = useState(0);
    const [persuasion, setPersuasion] = useState(0);
    const [streetwise, setStreetwise] = useState(0);
    const [subterfuge, setSubterfuge] = useState(0);
    const [academics, setAcademics] = useState(0);
    const [awareness, setAwareness] = useState(0);
    const [finance, setFinance] = useState(0);
    const [investigation, setInvestigation] = useState(0);
    const [medicine, setMedicine] = useState(0);
    const [occult, setOccult] = useState(0);
    const [politics, setPolitics] = useState(0);
    const [science, setScience] = useState(0);
    const [technology, setTechnology] = useState(0);

    return <>
        <SectionTitle title={"Skills"}/>
        <div className='skills-container'>
            <div className='skills-first-column'>
                <AttributeLine label='Athletics' size={radioSize} level={athletics} type={separatorType}></AttributeLine>
                <AttributeLine label='Brawl' size={radioSize} level={brawl} type={separatorType}></AttributeLine>
                <AttributeLine label='Craft' size={radioSize} level={craft} type={separatorType}></AttributeLine>
                <AttributeLine label='Drive' size={radioSize} level={drive} type={separatorType}></AttributeLine>
                <AttributeLine label='Firearms' size={radioSize} level={firearms} type={separatorType}></AttributeLine>
                <AttributeLine label='Melee' size={radioSize} level={melee} type={separatorType}></AttributeLine>
                <AttributeLine label='Larceny' size={radioSize} level={larceny} type={separatorType}></AttributeLine>
                <AttributeLine label='Stealth' size={radioSize} level={stealth} type={separatorType}></AttributeLine>
                <AttributeLine label='Survival' size={radioSize} level={survival} type={separatorType}></AttributeLine>
            </div>
            <div className='skills-second-column'>
                <AttributeLine label='Animal Ken' size={radioSize} level={animalKen} type={separatorType}></AttributeLine>
                <AttributeLine label='Etiquette' size={radioSize} level={etiquette} type={separatorType}></AttributeLine>
                <AttributeLine label='Insight' size={radioSize} level={insight} type={separatorType}></AttributeLine>
                <AttributeLine label='Intimidation' size={radioSize} level={intimidation} type={separatorType}></AttributeLine>
                <AttributeLine label='Leadership' size={radioSize} level={leadership} type={separatorType}></AttributeLine>
                <AttributeLine label='Performance' size={radioSize} level={performance} type={separatorType}></AttributeLine>
                <AttributeLine label='Persuasion' size={radioSize} level={persuasion} type={separatorType}></AttributeLine>
                <AttributeLine label='Streetwise' size={radioSize} level={streetwise} type={separatorType}></AttributeLine>
                <AttributeLine label='Subterfuge' size={radioSize} level={subterfuge} type={separatorType}></AttributeLine>
            </div>
            <div className='skills-third-column'>
                <AttributeLine label='Academics' size={radioSize} level={academics} type={separatorType}></AttributeLine>
                <AttributeLine label='Awareness' size={radioSize} level={awareness} type={separatorType}></AttributeLine>
                <AttributeLine label='Finance' size={radioSize} level={finance} type={separatorType}></AttributeLine>
                <AttributeLine label='Investigation' size={radioSize} level={investigation} type={separatorType}></AttributeLine>
                <AttributeLine label='Medicine' size={radioSize} level={medicine} type={separatorType}></AttributeLine>
                <AttributeLine label='Occult' size={radioSize} level={occult} type={separatorType}></AttributeLine>
                <AttributeLine label='Politics' size={radioSize} level={politics} type={separatorType}></AttributeLine>
                <AttributeLine label='Science' size={radioSize} level={science} type={separatorType}></AttributeLine>
                <AttributeLine label='Technology' size={radioSize} level={technology} type={separatorType}></AttributeLine>
            </div>
        </div>
    </>    
}
