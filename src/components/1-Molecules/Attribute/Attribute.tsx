import React from 'react'
import {Radio} from './../../0-Atoms/Radio/Radio'
import './Attribute.scss'

interface AttributeProps {
    size: number,
    level: number,
}

export const Attribute: React.FC<AttributeProps> = (props) => {
    let attributeRender = [];
    for (var i = 1; i <= (props.size); i++) {
        if (i <= (props.level)) {
            attributeRender.push(<Radio key={i} checked={true}/>);
            continue;
        }
        attributeRender.push(<Radio key={i}/>);
    }

    return <>
        <div className="radio-container">
            {attributeRender}
        </div>
    </>
}

Attribute.defaultProps = {
    size: 0,
    level: 0
}