import React from 'react';
import { Attribute } from './Attribute';

export default {
    title: 'Attribute',
    component: Attribute,
};

export const Empty = () => <Attribute size={5}/>;

export const WithLevel = () => (
    <Attribute size={5} level={3}/>
);