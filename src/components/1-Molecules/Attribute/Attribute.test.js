import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";

import {Attribute} from "./Attribute";

let container = null;
beforeEach(() => {
    // met en place un élément DOM comme cible de rendu
    container = document.createElement("div");
    document.body.appendChild(container);
});

afterEach(() => {
    // nettoie en sortie de test
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});

it("should display attribute with the right radios", () => {
    act(() => {
        render(<Attribute size={5} />, container);
    });
    expect(container.querySelectorAll(".radio-checkmark").length).toBe(5);

    act(() => {
        render(<Attribute size={5} level={3} />, container);
    });
    expect(container.querySelectorAll(".checkmark--checked").length).toBe(3);
    expect(container.querySelectorAll(".radio-checkmark").length).toBe(5);

});