import React from 'react';
import { Gauge } from './Gauge';

export default {
	title: 'Gauge',
	component: Gauge,
};

export const Empty = () => <Gauge size={10}/>;

export const WithLocked = () => (
	<Gauge size={10} length={8}/>
);

export const WithLightDamage = () => (
	<Gauge size={10} nbLight={3}/>
);

export const WithLightDamageAndLocked = () => (
	<Gauge size={10} nbLight={3} length={8}/>
);

export const WithHeavyDamage = () => (
	<Gauge size={10} nbHeavy={2}/>
);

export const WithHeavyDamageAndLocked = () => (
	<Gauge size={10} nbHeavy={2} length={8}/>
);

export const WithHeavyAndLightDamageAndLocked = () => (
	<Gauge size={10} nbHeavy={2} nbLight={3} length={8}/>
);