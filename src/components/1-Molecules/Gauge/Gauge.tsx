import React from 'react'
import {Checkbox} from './../../0-Atoms/Checkbox/Checkbox'
import './Gauge.scss'

interface GaugeProps {
    size: number,
    length: number,
    nbHeavy: number,
    nbLight: number,
}

export const Gauge: React.FC<GaugeProps> = (props) => {
    let gaugeRender = [];
    for (var i = 1; i <= (props.size); i++) {
        if (i <= (props.nbHeavy)) {
            gaugeRender.push(<Checkbox key={i} checked={true} currentState="heavy"/>);
            continue;
        }
        if (i > (props.nbHeavy) && i <= (props.nbHeavy) + (props.nbLight)) {
            gaugeRender.push(<Checkbox key={i} checked={true} currentState="light"/>);
            continue;
        }
        if ((props.length) !== 0 && i > (props.length) && i <= (props.size)) {
            gaugeRender.push(<Checkbox key={i} currentState="locked"/>);
            continue;
        }
        gaugeRender.push(<Checkbox key={i} currentState="default"/>);
    }

    return <>     
        <div className="checkbox-container">
            {gaugeRender}
        </div>
    </>
}

Gauge.defaultProps = {
    size: 0,
    length: 0,
    nbHeavy: 0,
    nbLight: 0
}