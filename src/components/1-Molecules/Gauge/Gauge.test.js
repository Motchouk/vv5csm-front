import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";

import {Gauge} from "./Gauge";

let container = null;
beforeEach(() => {
    // met en place un élément DOM comme cible de rendu
    container = document.createElement("div");
    document.body.appendChild(container);
});

afterEach(() => {
    // nettoie en sortie de test
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});

it("should display gauge with the right checkboxes", () => {
    act(() => {
        render(<Gauge size={10} />, container);
    });
    expect(container.querySelectorAll(".checkbox-checkmark").length).toBe(10);

    act(() => {
        render(<Gauge size={10} length={8} />, container);
    });
    expect(container.querySelectorAll(".checkmark--default").length).toBe(8);
    expect(container.querySelectorAll(".checkmark--locked").length).toBe(2);

    act(() => {
        render(<Gauge size={10} nbLight={3} />, container);
    });
    expect(container.querySelectorAll(".checkmark--default").length).toBe(7);
    expect(container.querySelectorAll(".checkmark--light").length).toBe(3);

    act(() => {
        render(<Gauge size={10} nbLight={3} length={8} />, container);
    });
    expect(container.querySelectorAll(".checkmark--default").length).toBe(5);
    expect(container.querySelectorAll(".checkmark--light").length).toBe(3);
    expect(container.querySelectorAll(".checkmark--locked").length).toBe(2);

    act(() => {
        render(<Gauge size={10} nbHeavy={2} />, container);
    });
    expect(container.querySelectorAll(".checkmark--default").length).toBe(8);
    expect(container.querySelectorAll(".checkmark--heavy").length).toBe(2);

    act(() => {
        render(<Gauge size={10} nbHeavy={2} length={8} />, container);
    });
    expect(container.querySelectorAll(".checkmark--default").length).toBe(6);
    expect(container.querySelectorAll(".checkmark--heavy").length).toBe(2);
    expect(container.querySelectorAll(".checkmark--locked").length).toBe(2);

    act(() => {
        render(<Gauge size={10} nbHeavy={2} nbLight={3} length={8} />, container);
    });
    expect(container.querySelectorAll(".checkmark--default").length).toBe(3);
    expect(container.querySelectorAll(".checkmark--light").length).toBe(3);
    expect(container.querySelectorAll(".checkmark--heavy").length).toBe(2);
    expect(container.querySelectorAll(".checkmark--locked").length).toBe(2);
});