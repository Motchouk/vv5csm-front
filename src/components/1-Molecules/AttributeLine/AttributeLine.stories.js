import React from 'react';
import { AttributeLine } from './AttributeLine';

export default {
    title: 'AttributeLine',
    component: AttributeLine,
};

export const Strength = () => <AttributeLine label='Strength' size={5} level={2}/>;

export const Intimidation = () => (
    <AttributeLine label='Intimidation' type='dotted' size={5} level={2}/>
);

export const Etiquette = () => (
    <AttributeLine label='Etiquette' type='dotted' size={5}/>
);