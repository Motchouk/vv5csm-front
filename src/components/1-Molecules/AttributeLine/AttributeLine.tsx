import React from 'react'
import {Attribute} from './../Attribute/Attribute'
import {Label} from './../../0-Atoms/Label/Label'
import {Separator} from './../../0-Atoms/Separator/Separator'
import './AttributeLine.scss'

interface AttributeLineProps {
    htmlFor?: string,
    label: string,
    size: number,
    level: number,
    type: string,
}

export const AttributeLine: React.FC<AttributeLineProps> = (props) => {
    return <> 
        <div className='attribute-line-container'>
            <Label htmlFor={props.htmlFor} label={props.label}></Label>
            <Separator type={props.type}/>
            <Attribute size={props.size} level={props.level}></Attribute>
        </div>
    </>
}

AttributeLine.defaultProps = {
    label: 'label',
    size: 0,
    level: 0,
    type: 'default'
}