import React from 'react'
import { Label } from './../../0-Atoms/Label/Label'
import { Message } from './../../0-Atoms/Message/Message'
import './FormField.scss'

interface FormFieldProps {
    htmlFor?: string,
    label: string,
    inputType: string,
    name: string,
    value: string,
    messageType: string,
    text: string,
    placeholder: string,
    onChange: any,
}

export const FormField: React.FC<FormFieldProps> = (props) => {
    return <>
        <div className='form-field-container'>
            <Label htmlFor={props.htmlFor} label={props.label}/>
            <input
                type={props.inputType}
                name={props.name}
                value={props.value}
                placeholder={props.placeholder}
                onChange={props.onChange}
            />
            <Message type={props.messageType} text={props.text}/>
        </div>
    </>
}

FormField.defaultProps = {
    label: 'label',
    inputType: 'text',
    name: '',
    value: '',
    messageType: "default",
    text: '',
}
