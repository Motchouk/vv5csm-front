import React from 'react';
import {FormField} from './FormField';

export default {
    title: 'FormField',
    component: FormField,
};

export const Email = () => <FormField label='Email' inputType='email' name='email' value='poutou@gmail.com' messageType="success" text='Votre email est valide.' />;

export const Password = () => <FormField label='Password' inputType='password' name='password' messageType="error" text='Votre mot de passe ne peut pas être vide.' />;