import React from 'react'
import './Message.scss'

interface MessageProps {
    type: string,
    text: string,
}

export const Message: React.FC<MessageProps> = (props) => {
    const { type } = props;
    const classPrefix = "message message--";
    let stateClass = classPrefix.concat(type);

    return <>
        <div className={stateClass}>{props.text}</div>
    </>
}

Message.defaultProps = {
    type: "default",
    text: ""
}