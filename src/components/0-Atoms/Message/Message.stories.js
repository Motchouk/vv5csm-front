import React from 'react';
import { Message } from './Message';

export default {
    title: 'Message',
    component: Message,
};

export const Success = () => <Message type={"success"} text={"Succès."}/>;
export const Error = () => <Message type={"error"} text={"Erreur."}/>;