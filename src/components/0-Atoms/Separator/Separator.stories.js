import React from 'react';
import { Separator } from './Separator';

export default {
  title: 'Separator',
  component: Separator,
};

export const Default = () => <Separator/>;

export const Dotted = () => <Separator type={"dotted"}/>;