import React from 'react'
import './Separator.scss'

interface SeparatorProps {
    type: string,
}

export const Separator: React.FC<SeparatorProps> = (props) => {
    const { type } = props;
    const classPrefix = "separator separator--";
    let stateClass = classPrefix.concat(type);
    
    return <>
        <div className={stateClass}></div>
    </>
};

Separator.defaultProps = {
    type: "default"
}