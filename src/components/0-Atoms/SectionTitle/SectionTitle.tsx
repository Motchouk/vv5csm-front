import React from 'react'
import './SectionTitle.scss'

interface SectionTitleProps {
    title: string,
}

export const SectionTitle: React.FC<SectionTitleProps> = (props) => {
    return <>
        <h2 className={"section-title"}>{props.title}</h2>
    </>
};

SectionTitle.defaultProps = {
    title: "default",
}