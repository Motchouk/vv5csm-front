import React from 'react';
import { SectionTitle } from './SectionTitle';

export default {
  title: 'SectionTitle',
  component: SectionTitle,
};

export const Default = () => <SectionTitle title={"default"}/>;

export const AttributesTitle = () => <SectionTitle title={"Attributes"}/>;