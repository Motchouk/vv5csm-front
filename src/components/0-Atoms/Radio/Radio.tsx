import React, { useCallback } from 'react'
import './Radio.scss'

interface RadioProps {
    checked?: boolean,
    type?: string
}

export const Radio: React.FC<RadioProps> = (props) => {
    let stateClass = props.checked ? "radio-checkmark checkmark--checked" : "radio-checkmark";

    const handleChange = useCallback(() => {},[]);

    return  <>
        <input {...props} onChange={handleChange} />
        <span className={stateClass}></span>
    </>
}

Radio.defaultProps = {
    type: "radio", // This value is adopted when name prop is omitted.
    checked: false
}