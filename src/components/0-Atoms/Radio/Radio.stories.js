import React from 'react';
import { action } from '@storybook/addon-actions';
import { Radio } from './Radio';

export default {
  title: 'Radio',
  component: Radio,
};

export const Default = () => <Radio onClick={action('clicked')}/>;

export const Checked = () => (
    <Radio onClick={action('clicked')} checked={true}/>
);
