import React, { useCallback } from 'react'
import './Checkbox.scss'

interface CheckboxProps {
    currentState: string,
    type?: string,
    checked?: boolean,
}


export const Checkbox: React.FC<CheckboxProps> = (props) => {
    const { currentState, ...rest } = props;
    const classPrefix = "checkbox-checkmark checkmark--";
    const stateClass =  classPrefix.concat(currentState);

    const handleChange = useCallback(() => {},[]);

    return <>
        <input {...rest} onChange={handleChange} />
        <span className={stateClass}></span> 
    </>
}

Checkbox.defaultProps = {
    type: "checkbox", // This value is adopted when name prop is omitted.
    currentState: "default",
    checked: false
}