import React from 'react';
import { action } from '@storybook/addon-actions';
import { Checkbox } from './Checkbox';

export default {
  title: 'Checkbox',
  component: Checkbox,
};

export const Default = () => <Checkbox onClick={action('clicked')}/>;

export const LightDamage = () => (
    <Checkbox onClick={action('clicked')} checked={true}  currentState={"light"}/>
);

export const HeavyDamage = () => (
    <Checkbox onClick={action('clicked')} checked={true}  currentState={"heavy"}/>
);

export const Locked = () => (
    <Checkbox onClick={action('clicked')} currentState={"locked"}/>
);