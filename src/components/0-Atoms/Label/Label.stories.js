import React from 'react';
import { Label } from './Label';

export default {
  title: 'Label',
  component: Label,
};

export const Default = () => <Label htmlFor={"for-something"} label={"label"}/>;