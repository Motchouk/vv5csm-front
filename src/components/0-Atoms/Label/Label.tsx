import React from 'react'
import './Label.scss'

interface LabelProps {
    htmlFor?: string,
    label: string,
}

export const Label: React.FC<LabelProps> = (props) => {
    return  <>
        <label htmlFor={props.htmlFor}>{props.label}</label>
    </>
}

Label.defaultProps = {
    label: "label",
}