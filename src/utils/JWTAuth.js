import axios from 'axios';

const login = async (loginFormData) => {
    const LOGIN_ENDPOINT = `${process.env.REACT_APP_API_BASE_URL}login`;
    console.log("login appelé.");

    await axios.post(LOGIN_ENDPOINT, loginFormData).then(response => {
        if (response.status === 200 && response.data.token){
            let token = response.data.token;
            localStorage.setItem("access_token", token);
            console.log("token OK.");
        }
        return response;
    });
   
}

const logout = () => {
    localStorage.removeItem("access_token");
}

export { login, logout}