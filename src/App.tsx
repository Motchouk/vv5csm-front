import React from 'react';
//import logo from './logo.svg';
import './App.css';
import { logout } from "./utils/JWTAuth.js";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import { Login } from './components/3-Pages/Login/Login';


class App extends React.Component {

  render() {
    console.log(`${process.env.REACT_APP_NAME}`)
    return (
      <Router>
        <Switch>
          <Route path="/login">
            <Login/>
          </Route>
          <div className="container">
            <div className="row">
              <h1>React JWT Authentication Example</h1>
                <button className="btn btn-primary">
                  <Link to="/login">Log in</Link>
                </button>
                <button className="btn btn-primary" onClick = { logout }>Log out</button>
            </div>
          </div>
        </Switch>
      </Router>
    );
  }
}

export default App;
